typedef enum loglvl_t {
    LOGLVL_DBG,
    LOGLVL_INFO,
    LOGLVL_WARN,
    LOGLVL_ERR,
} LOGLVL_T;

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

void sf_log(LOGLVL_T log_level, const char *fmt, ...);
void log_utils_set_level(LOGLVL_T log_level);
