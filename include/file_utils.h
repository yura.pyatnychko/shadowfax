// container sizes for storing information taken from files

void file_utils_info();
int file_utils_open(char* file_path_);
int file_utils_close();

int file_utils_get_primary_data(char **data);
int file_utils_get_secondary_data(char **data);
int file_utils_load_data();
int file_utils_display();
