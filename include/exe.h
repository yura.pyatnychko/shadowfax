#include <dyncall.h>

int exe_process ();
void exe_ring_init();

void exe_ring_push(DCCallVM* vm_, void *fp);
void exe_ring_pop();
int exe_ring_size();

int exe_ring_call();
int get_elapsed_time();
