#define CLI_ARGS_MAX_CNT  10
#define CLI_ARGS_MAX_LEN  50
#define CLI_LABEL_MAX_LEN 20

int cli_process ();

int cli_help_cmd(int tst0, int tst1);
int cli_settings_cmd(int loglvl);
int cli_quit_cmd();
int cli_file_cmd(char op, char* fp);
int encode_cmd(char *dna);
int decode_cmd(int dna, int len);

//int cli_file_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int w101_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int w102_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int w103_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int w104_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int w105_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);

//int sort_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);

//int encode_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
//int decode_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN]);
