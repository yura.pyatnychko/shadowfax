// uint8_t .. etc
#include <stdint.h>

#define MAX_STARTING_INDEXES 1000
#define MAX_PATTERN_LEN 100
#define MAX_DATA_LEN 5000000

int dna_utils_patternCount_RvsComp(char *data, int data_len,
                                       char *pattern, int pattern_len,
                                       int starting_indexes[MAX_STARTING_INDEXES]);

int dna_utils_patternCount(char *data, int data_len,
                               char *pattern, int pattern_len,
                               int starting_indexes[MAX_STARTING_INDEXES]);

void dna_utils_reverseComplement(char *dna_data_out, char *dna_data_in, int dna_data_len);


int dna_utils_find_clumps(char *dna_data, int dna_data_len,
                               int k, int L, int t);

void dna_utils_frequency_array(char* dna_data, int dna_data_len, int k);

// ENCODING/DECODING FUNCTIONS
uint8_t dna_utils_encode_char(char input);
char    dna_utils_decode_char(uint8_t input);

int     dna_utils_encode_string(char *dna_data, int dna_data_len);
char*   dna_utils_decode_string(char *decoded_data, int encoded_data_input, int dna_data_len);

void    merge_sort(int* data, int l, int r); 

int   dna_utils_encode_string_r(char *dna_data, int dna_data_len);
char* dna_utils_decode_string_r(char *decoded_data_out, int encoded_data_in, int dna_data_len);
