
// TODO implement core affinity
//#define _GNU_SOURCE
//#include <sched.h>
//#include <unistd.h>

#include <log_utils.h>
#include <cli.h>
#include <exe.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void *cli_thread(void *vargp)
{
    while(cli_process()) {}
    sf_log(LOGLVL_INFO,"cli_thread, exiting...\n");
}


void *exe_thread(void *vargp)
{
     while(exe_process()) {}
     sf_log(LOGLVL_INFO,"exe_thread, exiting...\n");
}

int main()
{
    log_utils_set_level(LOGLVL_DBG);
    exe_ring_init();

    pthread_t cli_tid; 
    pthread_t exe_tid;

    // TODO implement core affinity
    //cpu_set_t cpuset;
    //CPU_ZERO(&cpuset);



    if(pthread_create(&cli_tid, NULL, cli_thread, NULL) != 0) {
        perror("pthread_create(&cli_tid ...)");
        return -1;
    }

    if(pthread_create(&exe_tid, NULL, exe_thread, NULL)) {
        perror("pthread_create(&exe_tid ...)");
        return -1;
    }

    pthread_join(cli_tid, NULL);
    pthread_join(exe_tid, NULL);

    return(0);
}
