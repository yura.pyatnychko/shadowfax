#include <tests.h>

//malloc
#include <stdlib.h>

#include <log_utils.h>
#include <dna_utils.h>


#define DEFAULT_ELEMENT_COUNT 10
#define DEFAULT_ELEMENT_SPREAD 10

void tests_merge_sort(int specified_size) 
{
    int i;
    
    int number_of_elements;
    int *elements;

    int error = 0;

    number_of_elements = specified_size > 0 ? specified_size : DEFAULT_ELEMENT_COUNT + rand()%DEFAULT_ELEMENT_SPREAD;
    elements = malloc(sizeof(int)*number_of_elements);

    sf_log(LOGLVL_INFO, "starting: tests_merge_sort, number_of_elements=%d\n", number_of_elements);
    
    // POPULATE ELEMENTS WITH RANDOM VALUES
    sf_log(LOGLVL_DBG, "unsorted elements [");
    for(i = 0; i < number_of_elements; i++) {
        elements[i] = rand()%(number_of_elements*2);
        sf_log(LOGLVL_DBG, "%d, ", elements[i]);
    }
    sf_log(LOGLVL_DBG, "]\n");

    merge_sort(elements, 0, number_of_elements-1);


    sf_log(LOGLVL_DBG, "  sorted elements [");
    for(i = 0; i < number_of_elements; i++) {
        sf_log(LOGLVL_DBG, "%d, ", elements[i]);
        if(elements[i] > elements[i+1] && i+1 < number_of_elements) {
            error = 1;
        }
    }
    sf_log(LOGLVL_DBG, "]\n");
    free(elements);

    if(error) {
        sf_log(LOGLVL_ERR, "TEST FAILED\n");
    } else {
        sf_log(LOGLVL_INFO, "TEST PASSED\n");
    }
}
