// printf()
#include <stdio.h>
// strlen()
#include <string.h>
//pow(x,y)
#include <math.h>
//malloc
#include <stdlib.h>

#include "log_utils.h"
#include "dna_utils.h"


int dna_utils_patternCount_RvsComp(char *data, int data_len,
                                   char *pattern, int pattern_len,
                                   int starting_indexes[MAX_STARTING_INDEXES])
{
    int i;
    int match_cnt = 0;

    // reverse complement of pattern
    char rvs_comp[MAX_DATA_LEN];
    
    dna_utils_reverseComplement(rvs_comp, pattern, pattern_len);

    for(i = 0; i < data_len - pattern_len; i++) {
        
        if(strncmp(&data[i], pattern, pattern_len) == 0 ||
           strncmp(&data[i], rvs_comp, pattern_len) == 0) {
            if(match_cnt < MAX_STARTING_INDEXES && starting_indexes != NULL) {
                starting_indexes[match_cnt] = i;
            }
            match_cnt++;
        }
    }

    if(match_cnt >= MAX_STARTING_INDEXES && starting_indexes != NULL) {
        sf_log(LOGLVL_ERR, "match_cnt(%d) > MAX_STARTING_INDEXES(%d)!\n", data_len, MAX_STARTING_INDEXES);
    }
    return match_cnt;
}

int dna_utils_patternCount(char *data, int data_len,
                           char *pattern, int pattern_len,
                           int starting_indexes[MAX_STARTING_INDEXES])
{
    int i;
    int match_cnt = 0;

    for(i = 0; i < data_len - pattern_len; i++) {
        
        if(strncmp(&data[i], pattern, pattern_len) == 0) {
            if(match_cnt < MAX_STARTING_INDEXES && starting_indexes != NULL) {
                starting_indexes[match_cnt] = i;
            }
            match_cnt++;
        }
    }
    if(match_cnt >= MAX_STARTING_INDEXES && starting_indexes != NULL) {
        sf_log(LOGLVL_ERR, "match_cnt(%d) > MAX_STARTING_INDEXES(%d)!\n", data_len, MAX_STARTING_INDEXES);

    }
    return match_cnt;
}

char getComplement(char c) {
    switch(c) {
    case 'A':
        return 'T';
    case 'T':
        return 'A';
    case 'C':
        return 'G';
    case 'G':
        return 'C';
    default:
        return 'X';
    }
}

void dna_utils_reverseComplement(char *dna_data_out, char *dna_data_in, int dna_data_len)
{
    int i;
    for(i = 0; i < dna_data_len; i++) {
        dna_data_out[i] = getComplement(dna_data_in[dna_data_len-i-1]);
    }
    dna_data_out[dna_data_len] = '\0';
    return;
}




#define MAX_UNIQUE_CLUMPS 10000

// if a <k> length pattern occures at least <t> times within <L> sized window, recognize it as clump
int dna_utils_find_clumps(char *dna_data, int dna_data_len, int k, int L, int t)
{
    int i;
    
    int dna_data_upper_bound = dna_data_len-k;

    char unique_clumps[MAX_UNIQUE_CLUMPS*MAX_PATTERN_LEN];
    int unique_clump_cnt = 0;

    sf_log(LOGLVL_DBG, "dna_data_len=%d, k=%d, L=%d, t=%d\n",
           dna_data_len, k, L, t);

    for(i = 0; i < dna_data_upper_bound; i++) {

        int j;
        int window_upper_bound = i+L-k < dna_data_upper_bound ? i+L-k : dna_data_upper_bound;
        int match_cnt=0;

        for(j = i; j < window_upper_bound; j++) {
            if(memcmp(dna_data+i, dna_data+j, k) == 0) {
                match_cnt++;
            }
        }

        if(match_cnt >= t) {

            int u=0;
            int repeats = 0;
            while(u < unique_clump_cnt) {
                if(memcmp(dna_data+i, unique_clumps+(u*k), k) == 0) {
                    repeats = 1;
                    break;
                }
                u++;
            }

            if(repeats) {
                //sf_log(LOGLVL_DBG, "clump found: match_cnt=%d, string=", match_cnt);
            } else {
                memcpy(unique_clumps+(u*k), dna_data+i, k);
                unique_clump_cnt++;
                sf_log(LOGLVL_INFO, "unique clump found: unique_clump_cnt=%d, match_cnt=%d, string=", unique_clump_cnt,  match_cnt);
                if(unique_clump_cnt > MAX_UNIQUE_CLUMPS) {
                    sf_log(LOGLVL_ERR, "unique_clump_cnt(%d) exceeded MAX_UNIQUE_CLUMPS(%d)\n", unique_clump_cnt, MAX_UNIQUE_CLUMPS);
                }

                int c_iter;

                for(c_iter = 0; c_iter < k; c_iter++) {
                    sf_log(LOGLVL_INFO, "%c", dna_data[i+c_iter]);
                }
                sf_log(LOGLVL_INFO, "\n");
            }
        }
    }
    return unique_clump_cnt;
}


// A = 2'b00
// T = 2'b11
// C = 2'b01
// G = 2'b10
uint8_t dna_utils_encode_char(char input)
{
    switch(input) {
    case 'A':
        return 0;
    case 'C':
        return 1;
    case 'G':
        return 2;
    case 'T':
        return 3;
    default:
        sf_log(LOGLVL_ERR, "failed encoding of input: %c\n", input);
        return 0;
    }
}

char dna_utils_decode_char(uint8_t input) {
    switch(input) {
    case 0:
        return 'A';
    case 1:
        return 'C';
    case 2:
        return 'G';
    case 3:
        return 'T';
    default:
        return 'X';
    }
}

#define ENCODE_MAX_SIZE 4 // bytes
int dna_utils_encode_string(char *dna_data, int dna_data_len)
{

    int word_offset;
    int word_index;

    int i;
    uint8_t encoded_result[ENCODE_MAX_SIZE] = {0}; // arbitralily set max result to 4

    for(i = 0; i < dna_data_len; i++) {

        word_offset = i/4;
        word_index  = i%4;

        if(word_offset == ENCODE_MAX_SIZE) {
            sf_log(LOGLVL_ERR, "word_offset(%d) exceeded ENCODE_MAX_SIZE(%d)\n", word_offset, ENCODE_MAX_SIZE);
        }

        uint8_t clr_mask = ~(0x3 << (2*word_index));
        encoded_result[word_offset] &= clr_mask;

        uint8_t set_mask = dna_utils_encode_char(dna_data[dna_data_len-i-1]) << (2*word_index);
        encoded_result[word_offset] |= set_mask;
    }

    int val32 = ((uint32_t*)encoded_result)[0];
    return val32;
}

char* dna_utils_decode_string(char *decoded_data, int encoded_data_input, int dna_data_len)
{
    uint8_t *encoded_data = (uint8_t*)&encoded_data_input;

    //char decoded_data[17];

    int i;
    for(i = 0; i < dna_data_len; i++) {

        int index = (dna_data_len-i-1);
        int val = encoded_data[index/4] >> (2*(index%4));
        //printf("index(%d), val=%d, char=%c\n", index, val, dna_utils_decode_char(val & 0x3));

        decoded_data[i] = dna_utils_decode_char(val&0x3);
    }
    decoded_data[dna_data_len] = '\n';
    //sf_log(LOGLVL_INFO, "decoded_data=%s\n", decoded_data);
    return decoded_data;
}

// Recursive function for encoding character stream of data
int dna_utils_encode_string_r(char *dna_data, int dna_data_len)
{
    if(dna_data_len == 0) {
        return 0;
    } else {
        return 4*dna_utils_encode_string_r(dna_data, dna_data_len-1) +
            dna_utils_encode_char(dna_data[dna_data_len-1]);
    }
}


char* dna_utils_decode_string_r(char *decoded_data_out, int encoded_data_in, int dna_data_len)
{
    if(dna_data_len == 0) {
        decoded_data_out = "";
    } else {
        sprintf(decoded_data_out, "%s%c", 
                dna_utils_decode_string_r(decoded_data_out, encoded_data_in/4, dna_data_len-1), 
                dna_utils_decode_char(encoded_data_in%4));
    }
    return decoded_data_out;
}

void dna_utils_frequency_array(char* dna_data, int dna_data_len, int k)
{


    int *freq_array;
    int freq_array_size = pow(4, k);
    sf_log(LOGLVL_DBG, "freq_array_size=%d\n", freq_array_size);

    freq_array = (int*) malloc(sizeof(int)*freq_array_size);

    int i;
    
    for(i = 0; i < dna_data_len-k+1; i++) {

        sf_log(LOGLVL_DBG, "string=");
        int j;
        for(j=0;j<k;j++) {
            sf_log(LOGLVL_DBG, "%c", dna_data[i+j]);
        }
        sf_log(LOGLVL_DBG, ", val=%d\n", dna_utils_encode_string(dna_data+i, k));

        freq_array[dna_utils_encode_string(dna_data+i, k)]++;
    }

    for(i = 0; i < freq_array_size; i++) {
        sf_log(LOGLVL_INFO, "%d ", freq_array[i]);
    }
    sf_log(LOGLVL_INFO, "\n");
    free(freq_array);
}


void merge_sort(int* data, int l, int r) 
{

    if(r > l) {
        // SORT SUBSECTIONS
        int m = (r+l)/2;
        merge_sort(data, l, m);
        merge_sort(data, m+1, r);

        
        // MERGE SUBSECTIONS
        int *merged_data;;
        
        //printf("allocing merged_data for %d elements\n", r-l+1);
        merged_data = malloc(sizeof(int)*(r+1-l));

        int i_l     = l;
        int i_r     = m+1;
        int i_merge = 0;
        while(i_l < m+1 && i_r < r+1) {
            if(data[i_r] < data[i_l]) {
                merged_data[i_merge++] = data[i_r++]; 
            } else {
                merged_data[i_merge++] = data[i_l++];
            }
        }

        while(i_l < m+1) {
            merged_data[i_merge++] = data[i_l++];
        }
        while(i_r < r+1) {
            merged_data[i_merge++] = data[i_r++];
        }

        int i;
        for(i = l; i < r+1; i++) {
            data[i] = merged_data[i-l];
        }
        
        free(merged_data);
        return;
    } else {
        return;
    }
}

