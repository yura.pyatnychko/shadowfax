int patternCount(char *data, int data_len, char *pattern, int pattern_len)
{

    int i;
    int match_cnt = 0;

    for(i = 0; i < data_len - pattern_len; i++) {
        
        if(strncmp(&data[i], pattern, pattern_len) == 0) {
            match_cnt++;
        } 
    }
    return match_cnt;
}
