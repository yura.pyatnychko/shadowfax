#include <stdarg.h>
#include <stdio.h>
#include <log_utils.h>

LOGLVL_T curr_log_level;

void sf_log(LOGLVL_T log_level, const char *fmt, ...)
{
    va_list args;

    if(log_level >= curr_log_level) {
        va_start(args, fmt);

        switch(log_level) {
        case LOGLVL_DBG:
            printf("%s", KYEL);
            vprintf(fmt, args);
            break;
        case LOGLVL_INFO:
            printf("%s", KCYN);
            vprintf(fmt, args);
            break;
        case LOGLVL_ERR:
            printf("%s", KRED);
            vprintf(fmt, args);
            break;
        default:
            vprintf(fmt, args);
            break;
        }
        printf("%s", KNRM);

        va_end(args);
    }
}

void log_utils_set_level(LOGLVL_T log_level)
{
    if(log_level != -1) {
        curr_log_level = log_level;
    }

    sf_log(LOGLVL_INFO, "%s curr_log_level=%s\n", log_level != -1 ? " setting" : "", 
           curr_log_level == LOGLVL_DBG  ? "LOGLVL_DBG"  :
           curr_log_level == LOGLVL_INFO ? "LOGLVL_INFO" :
           curr_log_level == LOGLVL_WARN ? "LOGLVL_WARN" :
           curr_log_level == LOGLVL_ERR  ? "LOGLVL_ERR"  : "LOGLVL_UNKNOWN");
}
