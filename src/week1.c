#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "log_utils.h"
#include "dna_utils.h"
#include "file_utils.h"

#include "week1.h"

// pattern count
// Very simple problem of counting how many times are particular pattern occures in a string
int pattern_count_cmd(char *ptrn_)
{
    char* func_label = "pattern_count_cmd";

    char* primary_data;
    int   primary_data_len = 0;
    char* ptrn;
    int   ptrn_len = 0;
    
    int starting_indexes[MAX_STARTING_INDEXES];
    int match_cnt;
    
    if((primary_data_len = file_utils_get_primary_data(&primary_data)) == 0) {
        sf_log(LOGLVL_ERR, "%s, primary_data needs to be loaded for this function to execute\n", func_label);
        return 1;
    }

    if(strcmp(ptrn_, "*") == 0) {
        sf_log(LOGLVL_ERR, "%s, missing parameter, ptrn\n", func_label);
        return 1;
    } else {
        ptrn = ptrn_;
        ptrn_len = strlen(ptrn_);
    }
     
    match_cnt = dna_utils_patternCount(primary_data, primary_data_len,
                                       ptrn, ptrn_len,
                                       starting_indexes);

    sf_log(LOGLVL_INFO, "%s, match_cnt=%d\n", func_label, match_cnt);

    
    for(int i = 0; i < match_cnt && i < MAX_STARTING_INDEXES; i++) {
        sf_log(LOGLVL_INFO, "%d, ", starting_indexes[i]);

    }
    sf_log(LOGLVL_INFO, "\n");

    return 1;
}

// frequent words
int freq_words_cmd(int k_)
{
    char* func_label = "freq_words_cmd";

    // inputs
    char *primary_data;
    int  primary_data_len = 0;
    int  k;

    // generated results
    int i;
    int pattern_cnt;
    
    int max_cnt[100]     = { 0 };
    int max_cnt_idx[100] = { 0 };
    
    char info_str[100];


    // process inputs
    if((primary_data_len = file_utils_get_primary_data(&primary_data)) == 0) {
        sf_log(LOGLVL_ERR, "%s, primary_data needs to be loaded for this function to execute\n", func_label);
        return 1;
    }
    if(k_ == -1) {
        sf_log(LOGLVL_ERR, "%s, missing parameter, k\n", func_label);
        return 1;
    } else {
        k = k_;
    }

    sf_log(LOGLVL_DBG, "%s,  k=%d\n", func_label, k);

        
    // process
    for(i = 0; i < primary_data_len - k; i++) {
        pattern_cnt = dna_utils_patternCount(&primary_data[i], primary_data_len-i-k, &primary_data[i], k, NULL);
            

        if(pattern_cnt >= max_cnt[0]) {
            memmove(&max_cnt_idx[1], &max_cnt_idx[0], 99);
            memmove(&max_cnt[1], &max_cnt[0], 99);
            max_cnt[0] = pattern_cnt;
            max_cnt_idx[0] = i;
        }
    }

    // log results
    for(i = 0; max_cnt[i] >= max_cnt[0]; i++) {
        sf_log(LOGLVL_DBG, "%s, max_cnt_idx[%d]=%d, max_cnt=%d\n", func_label, i, max_cnt_idx[i], max_cnt[i]);
    }
    
    sf_log(LOGLVL_INFO, "%s, number of unique frequent words: %d\n", func_label, i);

    sf_log(LOGLVL_INFO, "%s, most freq_words: ", func_label);
    for(i = 0; max_cnt[i] >= max_cnt[0]; i++) {
        info_str[0] = '\0';
        strncat(info_str, &primary_data[max_cnt_idx[i]], k);
        sf_log(LOGLVL_INFO, "%s, ", info_str);
    }
    sf_log(LOGLVL_INFO, "\n");
    
    
    return 1;
}

// reverse compliment
int reverse_compliment_cmd()
{
    char *func_label = "reverse_compliment_cmd";

    char *primary_data;
    int  primary_data_len = 0;

    if((primary_data_len = file_utils_get_primary_data(&primary_data)) == 0) {
        sf_log(LOGLVL_ERR, "%s, primary_data needs to be loaded for this function to execute\n", func_label);
        return 1;
    }

    char reverse_complement[MAX_DATA_LEN];

    dna_utils_reverseComplement(reverse_complement, primary_data, primary_data_len);

    sf_log(LOGLVL_DBG, "%s, primary_data:\n%s\n", func_label, primary_data);
    sf_log(LOGLVL_DBG, "%s, primary_data_rvsComp:\n%s\n", func_label, reverse_complement);
}

// clump finder
int clump_finder_cmd(int k_, int L_, int t_)
{
    char *func_label = "clump_finder_cmd";

    char *primary_data;
    int  primary_data_len = 0;


    // default parameters set to
    // k=9
    // L=500
    // t=3
    int k = k_ != -1 ? k_ : 9;
    int L = L_ != -1 ? L_ : 500;
    int t = t_ != -1 ? t_ : 3;
    sf_log(LOGLVL_DBG, "%s k=%d L=%d t=%d\n", func_label, k, L, t);

    if((primary_data_len = file_utils_get_primary_data(&primary_data)) == 0) {
        sf_log(LOGLVL_ERR, "%s, primary_data needs to be loaded for this function to execute\n", func_label);
        return 1;
    }

    int unique_clump_cnt = dna_utils_find_clumps(primary_data, primary_data_len, k, L, t);

    sf_log(LOGLVL_INFO, "%s, unique_clump_cnt=%d\n", func_label, unique_clump_cnt);

    return 1;
}
