//dyncall
#include <dyncall.h>
// printf(), fgets()
#include <stdio.h>
// strlen(), strcpy(), strtok()
#include <string.h>
// uint8_t
#include <stdint.h>
// strtol
#include <stdlib.h>

//readline
#include <readline/readline.h>
#include <readline/history.h>

#include <stdbool.h>
#include <errno.h>
//#include <time.h>

#include <cli.h>
#include <exe.h>
#include <log_utils.h>
#include <file_utils.h>
#include <dna_utils.h>
#include <tests.h>

// Coursera exercises
#include <week1.h>


int debug_cmd();

typedef enum { 
    THREAD_CLI,
    THREAD_EXE
} thread_id_t;

typedef struct cli_cmd {
    char label[20];
    uint8_t thread_id;
    char description[100];
    char arg_format[200];
    void  *fp;
} cli_cmd_t;


cli_cmd_t cmd_table[] = {
    // command string, args, function pointer
    { "quit",         THREAD_CLI, "",                   "",                   &cli_quit_cmd           },
    { "help",         THREAD_CLI, "",                   "tst0=%i tst1=%i",    &cli_help_cmd           },
    { "settings",     THREAD_CLI, "",                   "loglvl=%i",          &cli_settings_cmd       },
    // I/O associated commands
    { "file",         THREAD_EXE, "",                   "op=%c file=%s",      &cli_file_cmd           },
    // data processing commands
    { "debug",        THREAD_EXE, "debug stub",         "",                   &debug_cmd              },
    { "elapsedTime",  THREAD_CLI, "get elapsed time",   "",                   &get_elapsed_time       }, //TODO: broken stub
    { "ptrnCount",    THREAD_EXE, "pattern count",      "ptrn=%s",            &pattern_count_cmd      },
    { "freqWords",    THREAD_EXE, "frequent words",     "k=%i",               &freq_words_cmd         },
    { "rvrsCompl",    THREAD_EXE, "reverse complement", "",                   &reverse_compliment_cmd },
    { "clumpFind",    THREAD_EXE, "clump finder",       "k=%i L=%i t=%i",     &clump_finder_cmd       },
    { "encode",       THREAD_EXE, "encode char->num",   "dna=%s",             &encode_cmd             },
    { "decode",       THREAD_EXE, "decode num->char",   "num=%i len=%i",      &decode_cmd             },

    //{ "w105",    THREAD_EXE, "freq array filler",  &w105_cmd },
    //{ "sort",    THREAD_EXE, ""                 ,  &sort_cmd },
};



/* A static variable for holding the line. */
static char *line_read = (char *)NULL;

/* Read a string, and return a pointer to it.  Returns NULL on EOF. */
char * rl_gets()
{
  /* If the buffer has already been allocated, return the memory
     to the free pool. */
  if(line_read) {
      free (line_read);
      line_read = (char *)NULL;
  }

  /* Get a line from the user. */
  line_read = readline (">> ");

  /* If the line has any text in it, save it on the history. */
  if (line_read && *line_read)
      add_history (line_read);

  return (line_read);
}

#define CLI_CMD_CNT sizeof(cmd_table)/sizeof(cli_cmd_t)
#define CLI_CMD_MAX_LEN 200

#define FUNC_ARG_INT_DEFAULT  -1
#define FUNC_ARG_CHAR_DEFAULT '*'
#define FUNC_ARG_STR_DEFAULT  "*"


int cli_process () 
{

    // 1. CAPTURE INPUT
    char * cli_str;
    
    // readline machanism
    cli_str = rl_gets();

    // 2. IDENTIFY FUNCTION
    char *inp_token, *inp_saveptr;

    inp_token = strtok_r(cli_str, " \n", &inp_saveptr);
    if(inp_token == NULL || inp_token[0] == '#') {
        return 1;
    }

    int funcIndex;
    char arg_format[200];

    for(funcIndex = 0; funcIndex < CLI_CMD_CNT; funcIndex++) {
        if(strcmp(inp_token, cmd_table[funcIndex].label) == 0) {
            strcpy(arg_format, cmd_table[funcIndex].arg_format); 
            break;
        }
    }
    
    if(funcIndex == CLI_CMD_CNT) {
        sf_log(LOGLVL_ERR,"function not found\n");
        return 1;
    }

    sf_log(LOGLVL_DBG, "cli_process, function found, %s %s\n", cmd_table[funcIndex].label, arg_format);
           
    // 3. INTERPOSE ARGUMENTS
    char *ref_token, *ref_saveptr;


    inp_token = strtok_r(      NULL, "=\n", &inp_saveptr);
    ref_token = strtok_r(arg_format, "=\n", &ref_saveptr);
           
    int dbg_cnt = 0;

    DCCallVM* vm = dcNewCallVM(4096); 
    dcMode(vm, DC_CALL_C_DEFAULT);
    dcReset(vm);


    sf_log(LOGLVL_DBG, "cli_process|%4s|%10s|%10s|\n", "arg", "ref_token", "inp_token");
    sf_log(LOGLVL_DBG, "cli_process|----|----------|----------|\n");

    while(1) {
        dbg_cnt++;

        if(ref_token == NULL) {
            if(inp_token != NULL) {
                sf_log(LOGLVL_ERR, "cli_process, extraneous arguments (%s)\n", inp_token);
                return 1;
            }
            break;
        }

        sf_log(LOGLVL_DBG, "cli_process|%4d|%10s|%10s|\n", dbg_cnt, ref_token, inp_token);


        bool default_arg = inp_token == NULL || strcmp(ref_token, inp_token) != 0;

        if(!default_arg) {
            inp_token = strtok_r(NULL, " \n", &inp_saveptr);
        }
        ref_token = strtok_r(NULL, " \n", &ref_saveptr);

        sf_log(LOGLVL_DBG, "cli_process|%4s|%10s|%10s|\n", "", ref_token, inp_token);
        sf_log(LOGLVL_DBG, "cli_process|----|----------|----------|\n");

        if(strcmp(ref_token, "%i") == 0) {
            int convertInt = FUNC_ARG_INT_DEFAULT;

            if(!default_arg) {
                convertInt = (int)strtol(inp_token, NULL, 10);
                if(convertInt == 0 && strcmp(inp_token, "0") != 0) {
                    sf_log(LOGLVL_ERR, "cli_process, invalid int conversion\n");
                    return 1;
                }
            }

            dcArgInt(vm, convertInt);

        } else if(strcmp(ref_token, "%c") == 0) {
            char convertChar = FUNC_ARG_CHAR_DEFAULT;

            if(!default_arg) {
                if(strlen(inp_token) > 1) {
                    sf_log(LOGLVL_ERR, "cli_process, invalid char conversion");
                    return 1;
                }
                convertChar = inp_token[0];
            }
            dcArgChar(vm, convertChar);

        } else if(strcmp(ref_token, "%s") == 0) {
            char *convertString = FUNC_ARG_STR_DEFAULT;

            if(!default_arg) {
                convertString = inp_token;
            }
            dcArgPointer(vm, convertString);

        } else {
            sf_log(LOGLVL_ERR, "cli_process, unrecognized format type(%s), supported types include \%i, \%c, \%s\n", ref_token);
            return 1;
        }

        ref_token = strtok_r(NULL,   "=\n", &ref_saveptr);
        if(!default_arg) {
            inp_token = strtok_r(NULL,   "=\n", &inp_saveptr);
        }
    }


    if(cmd_table[funcIndex].thread_id == THREAD_EXE) {
        exe_ring_push(vm, cmd_table[funcIndex].fp);
        sf_log(LOGLVL_DBG, "cli_process, exe_ring_push executed\n");
    } else {
        dcCallInt(vm, cmd_table[funcIndex].fp);
    }
    dcFree(vm);


    return 1;
}

int cli_quit_cmd()
{
    sf_log(LOGLVL_INFO,"Quiting..\n");
    return 0;
}

int cli_help_cmd(int tst0, int tst1) 
{
    sf_log(LOGLVL_DBG,"cli_help_cmd, tst0=%d, tst1=%d\n", tst0, tst1);
    
    sf_log(LOGLVL_INFO,"command list:\n");
    for(int i = 0; i < CLI_CMD_CNT; i++) {
        sf_log(LOGLVL_INFO," %20s| %s %s\n", cmd_table[i].description, cmd_table[i].label, cmd_table[i].arg_format);
    }

    return 1;
}

int cli_settings_cmd(int loglvl) 
{
    log_utils_set_level(loglvl);
    return 1;
} 


int cli_file_cmd(char op, char* file) 
{

    switch(op) {
    case 'o':
        file_utils_open(file);
        break;
    case 'c':
        file_utils_close();
        break;
    case 'l':
        file_utils_load_data(file);
        break;
    case '*':
        file_utils_display();
        break;
    default:
        sf_log(LOGLVL_ERR, "cli_file_cmd, unrecoginzed operation \"op=%c\"", op);
        return 1;
    }
    file_utils_info();

    return 1;
}

int debug_cmd()
{
    sf_log(LOGLVL_INFO, "debug_cmd, I <3 Victoria\n");
    return 1;
}

int encode_cmd(char* dna)
{
    int dna_len     = strlen(dna);
    int encoded_val = dna_utils_encode_string(dna, dna_len);

    sf_log(LOGLVL_DBG,  "dna=%s, dna_len=%d\n", dna, dna_len);
    sf_log(LOGLVL_INFO, "encoded_val = %d\n", encoded_val);
   
    return 1;
}

int decode_cmd(int dna, int dna_len)
{    
    char decoded_val[17];
    if(dna_len == -1) {
        dna_len=16;
    }
    
    dna_utils_decode_string(decoded_val, dna, dna_len);

    sf_log(LOGLVL_INFO, "decoded_val = %s\n", decoded_val);

    return 1;
}

/*

// reverse complement


// clump finder
int w104_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN])
{
    if(primary_data_len == 0) {
        printf("-E- primary_data needs to be loaded for this function to execute\n");
        return 1;
    }
    int parameters[3] = {9,500,3};
    int *k = &parameters[0];
    int *L = &parameters[1];
    int *t = &parameters[2];

    int i;
    if(argc == 3) {
        for(i = 0; i < argc; i++) {
            parameters[i] = atoi(argv[i]);
        }
    } else if(secondary_data_len > 0) {
        char *arg_from_file;
        int parse_error = 0;

        // TODO: encapsulate into function
        //int output_cnt = parameterize_data(parameters, secondary_data);
        i = 0;
        arg_from_file = strtok(secondary_data, " ");
        while(arg_from_file != NULL) {
            parameters[i] = atoi(arg_from_file);
            i++;
            arg_from_file = strtok(NULL, " ");
        }
        if(i != 3) {
            printf("-E- secondary_data not properly formatted, i=%d\n", i);
            return 1;
        }

    } else if(secondary_data_len == 0) {
        printf("-E- secondary_data needs to be loaded, or arguments need to be provided for this function to execute\n");
        return 1;
    }

    int unique_clump_cnt = dna_utils_find_clumps(primary_data, primary_data_len, *k, *L, *t);

    printf("unique_clump_cnt=%d\n", unique_clump_cnt);

    return 1;
}

// frequency array filler
int w105_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN])
{
    if(primary_data_len == 0) {
        printf("-E- primary_data needs to be loaded for this function to execute\n");
        return 1;
    }
    if(secondary_data_len == 0) {
        printf("-E- secondary_data needs to be loaded for this function to execute\n");
        return 1;
    }

    dna_utils_frequency_array(primary_data, primary_data_len, atoi(secondary_data));

    return 1;
}

int sort_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN])
{
    if(argc == 1) {
        tests_merge_sort(atoi(argv[0]));
    } else {
        tests_merge_sort(0);
    }
}


int decode_cmd(int argc, char argv[CLI_ARGS_MAX_CNT][CLI_ARGS_MAX_LEN])
{
    char decoded_val[16];

    int parse_error = 0;
    if(argc == 2) {
        int val = atoi(argv[0]);
        int cnt = atoi(argv[1]);
        printf("decoding, val=%d, cnt=%d\n", val, cnt);
        dna_utils_decode_string_r(decoded_val, val, cnt);
    } else {
        parse_error = 1;
    }

    if(parse_error) printf("-E- error parsing function arguments");
    else printf("decoded_val=%s\n", decoded_val);
    return 1;
}
*/
