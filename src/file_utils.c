#include <stdio.h>
#include <errno.h>
// PATH_MAX
#include <limits.h>
// realpath
#include <stdlib.h>

#include <file_utils.h>
#include <log_utils.h>

#define FILE_MODE_READ  0x4
#define FILE_MODE_WRITE 0x2

FILE *fp;
char file_path[PATH_MAX] = "";
int  file_mode=0;

void file_utils_info()
{
    //sf_log(LOGLVL_INFO,
    //       "%15s = %s\n", 
    //       "*fp", fp == NULL ? "NULL" : "SET");
    sf_log(LOGLVL_INFO,
           "%15s = %s\n", 
           "file_path", file_path);
}

int file_utils_open(char* file_path_) 
{
    sf_log(LOGLVL_INFO, "file_utils_open, file_path=%s\n", file_path_);

    if(realpath(file_path_, file_path)) {
        fp = fopen(file_path, "r");
    }

    if(errno) {
        perror("file_utils_open");
        sf_log(LOGLVL_ERR, "file_utils_open, failed\n");
        return -1;
    } else {
        return 0;
    }
}

int file_utils_close()
{
    if(fp != NULL) {
        fclose(fp);
        //file_path[0] = '\0';
        fp = NULL;
    }


    if(errno) {
        perror("file_utils_close");
        sf_log(LOGLVL_ERR, "file_utils_close, failed\n");
        return -1;
    } else {
        return 0;
    }
}


#define CLI_PRIMARY_DATA_MAX_LEN 5000000
#define CLI_SECONDARY_DATA_MAX_LEN 1000

char primary_data[CLI_PRIMARY_DATA_MAX_LEN];
int  primary_data_len = 0;

char secondary_data[CLI_SECONDARY_DATA_MAX_LEN];
int  secondary_data_len = 0;

int file_utils_get_primary_data(char **data)
{
    *data  = primary_data;
    return primary_data_len;
}

int file_utils_get_secondary_data(char **data)
{
    *data  = secondary_data;
    return secondary_data_len;
}


int file_utils_load_data(char *file_path_) 
{
    if(file_utils_open(file_path_) == -1) {
        return -1;
    } else {
        rewind(fp);
    }

    if(errno) {
        sf_log(LOGLVL_ERR, "file_utils_load_data - rewind");
        return -1;
    }

    // context = 1 : primary data
    // context = 2 : secondary data
    // '\n' switches context
    int context = 1;
    primary_data_len   = 0;
    secondary_data_len = 0;
    
    char c;
    
    while((c = fgetc(fp)) != EOF) {
        
        if(c == '\n') {
            context++;
            continue;
        }
        
        if(context == 1) {
            if(primary_data_len < CLI_PRIMARY_DATA_MAX_LEN)
                primary_data[primary_data_len] = c;
            primary_data_len++;
        } else if(context == 2) {
            if(secondary_data_len < CLI_SECONDARY_DATA_MAX_LEN)
                secondary_data[secondary_data_len] = c;
            secondary_data_len++;
        }
    }
    
    primary_data[primary_data_len] = '\0';
    secondary_data[secondary_data_len] = '\0';
    
    sf_log(LOGLVL_DBG, "file_utils_load_data, primary_data_len=%d, secondary_data_len=%d\n", primary_data_len, secondary_data_len);

    return file_utils_close();
}

int file_utils_display()
{
    char *func_label = "file_utils_display";
   
    sf_log(LOGLVL_INFO, "%s, primary_data=%s, secondary_data=%s\n", func_label, primary_data, secondary_data);
    sf_log(LOGLVL_INFO, "%s, primary_data_len=%d, secondary_data_len=%d\n", func_label, primary_data_len, secondary_data_len);
}
