#include <dyncall.h>
#include <log_utils.h>
#include <time.h>

#include <stdio.h>
#include <string.h>

typedef struct exe_cmd {
    char label[20];
    DCCallVM* vm;
    void* fp;
} exe_cmd_t;


#define EXE_RING_SIZE 10

exe_cmd_t exe_ring[EXE_RING_SIZE];
int exe_ring_head = 0;
int exe_ring_tail = 0;

void exe_ring_init()
{
    for(int i = 0; i < EXE_RING_SIZE; i++) {
        exe_ring[i].vm = dcNewCallVM(4096);
        dcMode(exe_ring[i].vm, DC_CALL_C_DEFAULT);
    }
}

int exe_ring_call()
{
    dcCallInt(exe_ring[exe_ring_head].vm, exe_ring[exe_ring_head].fp);
}

void exe_ring_push(DCCallVM* vm_, void *fp)
{
    memcpy(&(exe_ring[exe_ring_tail].vm), &vm_, sizeof(&vm_));
    exe_ring[exe_ring_tail].fp = fp;

    exe_ring_tail = (exe_ring_tail + 1)%EXE_RING_SIZE;
    sf_log(LOGLVL_DBG, "exe_ring_push, called\n");
}

void exe_ring_pop()
{
    exe_ring_head = (exe_ring_head + 1)%EXE_RING_SIZE;
}

int exe_ring_size()
{
    return (EXE_RING_SIZE + exe_ring_tail - exe_ring_head)%EXE_RING_SIZE;
}

clock_t start, end = 0;

int exe_process () 
{
    int ring_size = exe_ring_size();

    if(ring_size > 0) {
        sf_log(LOGLVL_DBG, "exe_process, exe_ring_size()=%d, exe_ring_head=%d, exe_ring_tail=%d\n", ring_size, exe_ring_head, exe_ring_tail);


        start = clock();

        exe_ring_call();
        end  = clock();

        sf_log(LOGLVL_DBG, "exe_process, exe_ring_called() complete, cpu_time=%d\n", (int)(end-start));


        exe_ring_pop();
        sf_log(LOGLVL_DBG, "exe_process, exe_ring_pop() complete\n");

    }
    return 1;
}

int get_elapsed_time()
{
    sf_log(LOGLVL_DBG, "get_elapsed_time, (%d)\n", (int)((end-start)/CLOCKS_PER_SEC));
    return (int) ((end-start)/CLOCKS_PER_SEC);
}
