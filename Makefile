SDIR=src
IDIR=include
ODIR=obj
LDIR=lib

CC     = gcc
CFLAGS = -std=c99 -std=gnu99 -I$(IDIR) -lpthread -ldyncall_s -lreadline

LIBS=-lm

_DEPS = cli.h exe.h dna_utils.h log_utils.h tests.h file_utils.h week1.h
DEPS  = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = shadowfax.o cli.o exe.o dna_utils.o  log_utils.o tests.o file_utils.o week1.o
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

shadowfax: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)


.PHONY: clean

clean:
	rm -f shadowfax $(ODIR)/*.o *~ core $(IDIR)/*~
